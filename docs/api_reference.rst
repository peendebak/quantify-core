.. jupyter-kernel:: python3
    :id: quantify_core_all_docs

=============
quantify_core
=============

.. automodule:: quantify_core
   :members:

.. _analysis_api:

analysis
=============

.. automodule:: quantify_core.analysis
    :members:


base_analysis
-------------

.. automodule:: quantify_core.analysis.base_analysis
    :members:
    :show-inheritance:

cosine_analysis
---------------

.. automodule:: quantify_core.analysis.cosine_analysis
    :members:
    :show-inheritance:

spectroscopy_analysis
---------------------

.. automodule:: quantify_core.analysis.spectroscopy_analysis
    :members:
    :show-inheritance:

single_qubit_timedomain
-----------------------

.. automodule:: quantify_core.analysis.single_qubit_timedomain
    :members:
    :show-inheritance:


interpolation_analysis
----------------------

.. automodule:: quantify_core.analysis.interpolation_analysis
    :members:
    :show-inheritance:

optimization_analysis
----------------------

.. automodule:: quantify_core.analysis.optimization_analysis
    :members:
    :show-inheritance:


fitting_models
--------------

.. automodule:: quantify_core.analysis.fitting_models
    :members:
    :show-inheritance:


calibration
-----------

.. automodule:: quantify_core.analysis.calibration
    :members:
    :show-inheritance:


data
====

types
-----

.. automodule:: quantify_core.data.types
    :members:

handling
--------

.. automodule:: quantify_core.data.handling
    :members:


measurement
===========

.. automodule:: quantify_core.measurement
    :members:


utilities
=========

experiment_helpers
------------------

.. automodule:: quantify_core.utilities.experiment_helpers
    :members:

examples_support
----------------

.. automodule:: quantify_core.utilities.examples_support
    :members:

visualization
=============

.. automodule:: quantify_core.visualization
    :members:

color_utilities
---------------

.. automodule:: quantify_core.visualization.color_utilities
    :members:


mpl_plotting
------------

.. automodule:: quantify_core.visualization.mpl_plotting
    :members:

plot_interpolation
------------------

.. automodule:: quantify_core.visualization.plot_interpolation
    :members:


SI Utilities
------------

.. automodule:: quantify_core.visualization.SI_utilities
    :members:

============
bibliography
============

.. bibliography::
